Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: compiler_builtins
Upstream-Contact: Jorge Aparicio <japaricious@gmail.com>
Source: https://github.com/rust-lang-nursery/compiler-builtins

Files: *
Copyright:
 2018-2019 Jorge Aparicio <japaricious@gmail.com>
 2005-2011 David Schultz <das@FreeBSD.ORG>
 2017 The Rust Project Developers
License: MIT or Apache-2.0
Comment:
 Some file headers make reference to other copyright holders but they are in
 fact copyright holders for the original C files from which this project is
 derived (LLVM and libm), none of which remain in this source code. So those
 original copyright holders do not hold copyright on this derivative.
 .
 See discussion:
 https://github.com/rust-lang/compiler-builtins/issues/307
 https://github.com/rust-lang/libm/issues/215
 .
 README.md also mentions references to the "University of Illinois "BSD-Like"
 license" which is an older name for the NCSA license, which is the old license
 of the compiler-rt project, part of the LLVM project, from which this project
 is derived. Again, this information is obsolete - no part of compiler-rt
 exists today in this crate, and so the older copyright holders and license do
 not actually apply to the current version of the code.
 .
 In summary, MIT or Apache-2.0 is therefore the correct license for this crate,
 as well as the portions of it sourced from the libm crate.

Files: debian/*
Copyright:
 2019 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2019 kpcyrd <git@rxv.cc>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
